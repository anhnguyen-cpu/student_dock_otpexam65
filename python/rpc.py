import MySQLdb
from nameko.rpc import rpc

def connect():
    DBconnect = MySQLdb.connect(host='db',
                     user='devops',
                     passwd='devops101',
                     db='devops_db',
                     port=3306)
    return DBconnect

def insert(firstname, lastname, email, password):
    DBconnect = connect()
    cur = DBconnect.cursor()
    cur.execute("INSERT INTO student (FirstName, LastName, Email, Password) VALUES (%s, %s, %s, %s);", (firstname, lastname, email, password))
    id = cur.lastrowid
    DBconnect.commit()
    DBconnect.close()
    return id

class Service:
    name = "student"

    @rpc
    def insert(self, firstname, lastname, email, password):
        result = insert(firstname, lastname, email, password)
        return result